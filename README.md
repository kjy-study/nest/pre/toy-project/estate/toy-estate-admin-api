# Prisma
schema.prisma -> DB에 마이그레이션
```
npx prisma migrate dev --name <migrantion name>
```

schema.prisma -> 마이그레이션 쿼리만 생성(DB 적용 안함)
```
npx prisma migrate dev --create-only
npx prisma migrate dev --create-only --name <migrantion name>
```

schema.prisma -> 백엔드 type에 적용
```
npx prisma generate 
```

DB 스키마 -> schema.prisma 파일로 읽어오기(사용하지 말기)
> 되도록 사용하지 말자
> 스키마 파일의 model 이름의 첫글자가 소문자로 되고, 모델 내부의 필드명도 camelCase에서 SNAKE_CASE로 변경된다.
> Enum 참고하는 구문도 변경되니 절대 절대 사용 X
```
npx prisma db pull
```

프로덕션 환경에 마이그레이션 설정하기(https://www.prisma.io/docs/guides/database/developing-with-prisma-migrate/add-prisma-migrate-to-a-project)
마이그레이션 리스트에서 각각 마이그레이션 별로 적용해야 한다면
```
prisma migrate resolve --applied <마이그레이션 폴더명>
prisma migrate resolve --applied 20210426141759_initial-migration
```

# Common Package
* .npmrc 파일 생성
  ```
  @mion:registry=https://gitlab.com/api/v4/projects/39361361/packages/npm/
  //gitlab.com/api/v4/projects/39361361/packages/npm/:_authToken=LxEiS2-1byQ-Jfn_zakA
  ```

* install `npm i @mion/toy-estate-lib`
* update `npm update @mion/toy-estate-lib`
