import { PrismaService } from '@mion/toy-estate-lib';
import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { Code } from '@prisma/client';
import copy from 'fast-copy';
import request from 'supertest';
import { AppModule } from '../../src/app.module';
import { CodesExceptionMessage } from '../../src/codes/exceptions/codes.exception.message';
import { setGlobalOption } from '../../src/main';
import { EditUserInput } from '../../src/users/args/edit-user.input';

jest.setTimeout(10 * 60 * 1000); // 디버깅 시 timeOut이 발생되어 추가했습니다.(default: 5000ms)

describe('CodeController (e2e)', () => {
  let app: INestApplication;
  let accessToken: string;
  let prisma: PrismaService;
  const gqlEndpoint = '/graphql';
  const savedCodes: Code[] = [];

  beforeEach(async () => {
    // e2e 테스트를 위해 AppModule를 불러와 의존성 주입 합니다.
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();

    await setGlobalOption(app);

    prisma = moduleFixture.get<PrismaService>(PrismaService);

    await app.init();
    // await app.listen(80);
  });

  afterEach(async () => {
    await app.close();
  });

  afterAll(async () => {
    for (const savedCode of savedCodes) {
      await prisma.code.delete({
        where: {
          codeGroup_code: {
            codeGroup: savedCode.codeGroup,
            code: savedCode.code,
          },
        },
      });
    }
  });

  it('토큰 발급 (e2e)', async () => {
    const query = `
    mutation CreateToken($data: LoginInput!) {
      createToken(data: $data) {
        accessToken
        refreshToken
      }
    }
    `;

    const variables = {
      data: {
        email: 'mion@gmail.com',
        password: 'password1234!',
      },
    };

    const data = await request(app.getHttpServer())
      .post(gqlEndpoint)
      .send({ query, variables })
      .expect(200);

    expect(data.body.data.createToken).toHaveProperty(['accessToken']);
    expect(data.body.data.createToken).toHaveProperty(['refreshToken']);

    accessToken = data.body.data.createToken.accessToken;

    return data;
  });

  describe('코드 등록 (e2e)', () => {
    const query = `
    mutation CreateCode($createCodeInput: CreateCodeInput!) {
      createCode(createCodeInput: $createCodeInput) {
        code
        codeGroup
        codeNm
      }
    }
    `;

    const variables = {
      createCodeInput: {
        code: `TEST-${(Math.random() * 100).toFixed(0)}`,
        codeGroup: 'e2e_test',
        codeNm: 'e2e test',
      },
    };

    const reqEditBody = new EditUserInput();
    reqEditBody.name = 'editedMion';

    it('코드 등록', async () => {
      const data = await request(app.getHttpServer())
        .post(gqlEndpoint)
        .set('Authorization', `Bearer ${accessToken}`)
        .send({
          query,
          variables,
        })
        .expect(200);

      const savedCode = await prisma.code.findUnique({
        where: {
          codeGroup_code: {
            codeGroup: variables.createCodeInput.codeGroup,
            code: variables.createCodeInput.code,
          },
        },
      });

      expect(savedCode).not.toBeNull();

      savedCodes.push(savedCode);
    });

    it('이미 등록되어있는 코드 등록 시 에러 반환', async () => {
      const { body } = await request(app.getHttpServer())
        .post(gqlEndpoint)
        .set('Authorization', `Bearer ${accessToken}`)
        .send({
          query,
          variables,
        })
        .expect(200);

      const error = body.errors[0].extensions.response;

      expect(error.message).toEqual(
        CodesExceptionMessage.DuplicateCode(
          variables.createCodeInput.codeGroup,
          variables.createCodeInput.code,
        ),
      );

      return body;
    });
  });

  describe('코드 수정 (e2e)', () => {
    const query = `
    mutation EditCode($editCodeInput: EditCodeInput!) {
      editCode(editCodeInput: $editCodeInput) {
        code
        codeGroup
        codeNm
      }
    }    
    `;

    const variables = {
      editCodeInput: {
        code: 'TEST-014',
        codeGroup: 'TEST',
        codeNm: '수정된 값',
      },
    };

    it('코드 그룹이 없는 데이터 수정 시 오류 반환', async () => {
      const val = copy(variables);
      val.editCodeInput.codeGroup = 'unknown';
      val.editCodeInput.code = 'unknown';

      const { body } = await request(app.getHttpServer())
        .post(gqlEndpoint)
        .set('Authorization', `Bearer ${accessToken}`)
        .send({
          query,
          variables: val,
        })
        .expect(200);

      const error = body.errors[0].extensions.response;

      expect(error.message).toEqual(
        CodesExceptionMessage.NotExistsCodeGroup(val.editCodeInput.codeGroup),
      );
    });

    it('코드가 없는 데이터 수정 시 오류 반환', async () => {
      const val = copy(variables);
      val.editCodeInput.codeGroup = savedCodes[0].codeGroup;
      val.editCodeInput.code = 'unknown';

      const { body } = await request(app.getHttpServer())
        .post(gqlEndpoint)
        .set('Authorization', `Bearer ${accessToken}`)
        .send({
          query,
          variables: val,
        })
        .expect(200);

      const error = body.errors[0].extensions.response;

      expect(error.message).toEqual(
        CodesExceptionMessage.NotExistsCode(
          val.editCodeInput.codeGroup,
          val.editCodeInput.code,
        ),
      );
    });

    it('정상 수정', async () => {
      const val = copy(variables);
      val.editCodeInput.codeGroup = savedCodes[0].codeGroup;
      val.editCodeInput.code = savedCodes[0].code;

      const { body } = await request(app.getHttpServer())
        .post(gqlEndpoint)
        .set('Authorization', `Bearer ${accessToken}`)
        .send({
          query,
          variables: val,
        })
        .expect(200);

      const editedCode = await prisma.code.findUnique({
        where: {
          codeGroup_code: {
            codeGroup: val.editCodeInput.codeGroup,
            code: val.editCodeInput.code,
          },
        },
      });

      expect(editedCode.codeNm).toEqual(val.editCodeInput.codeNm);
    });
  });
});
