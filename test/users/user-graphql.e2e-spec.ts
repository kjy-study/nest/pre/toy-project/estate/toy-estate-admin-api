import { PrismaService } from '@mion/toy-estate-lib';
import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { User } from '@prisma/client';
import copy from 'fast-copy';
import request from 'supertest';
import { AppModule } from '../../src/app.module';
import { setGlobalOption } from '../../src/main';
import { UserExceptionMessage } from '../../src/users/exceptions/users.exception.message';

jest.setTimeout(10 * 60 * 1000); // 디버깅 시 timeOut이 발생되어 추가했습니다.(default: 5000ms)

describe('UserController (e2e)', () => {
  let app: INestApplication;
  let accessToken: string;
  let anotherAccessToken: string;
  let prisma: PrismaService;
  const gqlEndpoint = '/graphql';
  const savedUsers: User[] = [];

  beforeEach(async () => {
    // e2e 테스트를 위해 AppModule를 불러와 의존성 주입 합니다.
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();

    await setGlobalOption(app);

    prisma = moduleFixture.get<PrismaService>(PrismaService);

    await app.init();
    // await app.listen(80);
  });

  afterEach(async () => {
    await app.close();
  });

  afterAll(async () => {
    for (const savedUser of savedUsers) {
      await prisma.user.delete({
        where: {
          id: savedUser.id,
        },
      });
    }
  });

  describe('사용자 등록 (e2e)', () => {
    const query = `
    mutation Mutation($createUserInput: CreateUserInput!) {
      createUser(createUserInput: $createUserInput) {
        id
        email
        name
      }
    }    
    `;

    const variables = {
      createUserInput: {
        email: `${Math.random()}mion@gmail.com`,
        name: '테스트 데이터',
        password: 'password1234!',
      },
    };

    it('비밀번호 정규식이 다를경우(특수문자 없을 때)', async () => {
      const val = copy(variables);

      val.createUserInput.password = 'password1234';

      const data = await request(app.getHttpServer())
        .post(gqlEndpoint)
        .set('Authorization', `Bearer ${accessToken}`)
        .send({
          query,
          variables: val,
        })
        .expect(200);

      const error = data.body.errors[0].extensions.response;

      expect(error.message[0]).toEqual(
        UserExceptionMessage.InvalidPasswordForm(),
      );

      return data;
    });

    it('비밀번호 정규식이 다를경우(숫자 없을 때)', async () => {
      const val = copy(variables);

      val.createUserInput.password = 'password!@#$';

      const data = await request(app.getHttpServer())
        .post(gqlEndpoint)
        .set('Authorization', `Bearer ${accessToken}`)
        .send({
          query,
          variables: val,
        })
        .expect(200);

      const error = data.body.errors[0].extensions.response;

      expect(error.message[0]).toEqual(
        UserExceptionMessage.InvalidPasswordForm(),
      );

      return data;
    });

    it('비밀번호 정규식이 다를경우(8자리 안될 때)', async () => {
      const val = copy(variables);

      val.createUserInput.password = 'pa@#$';
      const data = await request(app.getHttpServer())
        .post(gqlEndpoint)
        .set('Authorization', `Bearer ${accessToken}`)
        .send({
          query,
          variables: val,
        })
        .expect(200);

      const error = data.body.errors[0].extensions.response;

      expect(error.message[0]).toEqual(
        UserExceptionMessage.InvalidPasswordForm(),
      );

      return data;
    });

    it('사용자 등록', async () => {
      const data = await request(app.getHttpServer())
        .post(gqlEndpoint)
        .set('Authorization', `Bearer ${accessToken}`)
        .send({ query, variables })
        .expect(200);

      const savedUser = await prisma.user.findUnique({
        where: {
          id: data.body.data.createUser.id,
        },
      });
      savedUsers.push(savedUser);
      return data;
    });

    it('중복된 이메일로 사용자 가입 시 에러 반환', async () => {
      const { body } = await request(app.getHttpServer())
        .post(gqlEndpoint)
        .set('Authorization', `Bearer ${accessToken}`)
        .send({ query, variables })
        .expect(200);

      const error = body.errors[0].extensions.response;
      expect(error.message).toEqual(
        UserExceptionMessage.DuplicateEmail(variables.createUserInput.email),
      );

      return body;
    });

    it('토큰 발급 (e2e)', async () => {
      const query = `
      mutation CreateToken($data: LoginInput!) {
        createToken(data: $data) {
          accessToken
          refreshToken
        }
      }
      `;

      const variables = {
        data: {
          email: `${savedUsers[0].email}`,
          password: 'password1234!',
        },
      };

      const data = await request(app.getHttpServer())
        .post(gqlEndpoint)
        .send({ query, variables })
        .expect(200);

      expect(data.body.data.createToken).toHaveProperty(['accessToken']);
      expect(data.body.data.createToken).toHaveProperty(['refreshToken']);

      accessToken = data.body.data.createToken.accessToken;

      return data;
    });
  });
  describe('사용자 수정 (e2e)', () => {
    const query = `
    mutation EditUser($editUserInput: EditUserInput!) {
      editUser(editUserInput: $editUserInput) {
        id
        email
        name
      }
    }
    `;

    const variables = {
      editUserInput: {
        name: '수정된 이름',
        userId: 1,
      },
    };

    it('다른 사용자가 사용자 정보를 수정 시 에러 반환', async () => {
      // 다른 사용자로 jwt 토큰 얻기
      const createTokenQuery = `
      mutation CreateToken($data: LoginInput!) {
        createToken(data: $data) {
          accessToken
          refreshToken
        }
      }
      `;

      const createTokenVariables = {
        data: {
          email: 'mion@gmail.com',
          password: 'password1234!',
        },
      };

      const createTokenResponse = await request(app.getHttpServer())
        .post(gqlEndpoint)
        .send({ query: createTokenQuery, variables: createTokenVariables })
        .expect(200);

      expect(createTokenResponse.body.data.createToken).toHaveProperty([
        'accessToken',
      ]);
      expect(createTokenResponse.body.data.createToken).toHaveProperty([
        'refreshToken',
      ]);

      anotherAccessToken =
        createTokenResponse.body.data.createToken.accessToken;

      const val = copy(variables);
      val.editUserInput.userId = savedUsers[0].id;
      // 사용자 정보 수정 테스트
      const { body } = await request(app.getHttpServer())
        .post(gqlEndpoint)
        .set('Authorization', `Bearer ${anotherAccessToken}`)
        .send({ query, variables: val })
        .expect(200);

      const error = body.errors[0].extensions.response;
      expect(error.message).toEqual(
        UserExceptionMessage.NotMatchUserId(
          savedUsers[0].email,
          createTokenVariables.data.email,
        ),
      );
    });

    it('사용자 정보 수정', async () => {
      variables.editUserInput.userId = savedUsers[0].id;

      const data = await request(app.getHttpServer())
        .post(gqlEndpoint)
        .set('Authorization', `Bearer ${accessToken}`)
        .send({ query, variables })
        .expect(200);

      const editedUser = await prisma.user.findUnique({
        where: {
          id: data.body.data.editUser.id,
        },
      });

      expect(editedUser.name).toEqual(variables.editUserInput.name);
    });
  });
});
