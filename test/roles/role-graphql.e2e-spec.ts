import { PrismaService } from '@mion/toy-estate-lib';
import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { Role, RoleTypes } from '@prisma/client';
import request from 'supertest';
import { AppModule } from '../../src/app.module';
import { setGlobalOption } from '../../src/main';
import { RolesExceptionMessage } from '../../src/roles/exceptions/roles.exception.message';

jest.setTimeout(10 * 60 * 1000); // 디버깅 시 timeOut이 발생되어 추가했습니다.(default: 5000ms)

describe('RoleController (e2e)', () => {
  let app: INestApplication;
  let accessToken: string;
  let prisma: PrismaService;
  const gqlEndpoint = '/graphql';
  const savedRoles: Role[] = [];

  beforeEach(async () => {
    // e2e 테스트를 위해 AppModule를 불러와 의존성 주입 합니다.
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();

    await setGlobalOption(app);

    prisma = moduleFixture.get<PrismaService>(PrismaService);

    await app.init();
  });

  afterEach(async () => {
    await app.close();
  });

  afterAll(async () => {
    for (const savedRole of savedRoles) {
      const findRole = await prisma.role.findUnique({
        where: {
          role: savedRole.role,
        },
      });

      if (findRole) {
        await prisma.role.delete({
          where: {
            role: savedRole.role,
          },
        });
      }
    }
  });

  it('토큰 발급 (e2e)', async () => {
    const query = `
    mutation CreateToken($data: LoginInput!) {
      createToken(data: $data) {
        accessToken
        refreshToken
      }
    }
    `;

    const variables = {
      data: {
        email: 'mion@gmail.com',
        password: 'password1234!',
      },
    };

    const data = await request(app.getHttpServer())
      .post(gqlEndpoint)
      .send({ query, variables })
      .expect(200);

    expect(data.body.data.createToken).toHaveProperty(['accessToken']);
    expect(data.body.data.createToken).toHaveProperty(['refreshToken']);

    accessToken = data.body.data.createToken.accessToken;

    return data;
  });

  describe('롤 등록 (e2e)', () => {
    const query = `
    mutation CreateRole($createRoleInput: CreateRoleInput!) {
      createRole(createRoleInput: $createRoleInput) {
        role
      }
    }
    `;

    const variables = {
      createRoleInput: {
        role: RoleTypes.TEST,
      },
    };

    it('롤 등록', async () => {
      const data = await request(app.getHttpServer())
        .post(gqlEndpoint)
        .set('Authorization', `Bearer ${accessToken}`)
        .send({ query, variables })
        .expect(200);

      const responseRole = data.body.data.createRole;
      const savedRole = await prisma.role.findUnique({
        where: {
          role: variables.createRoleInput.role,
        },
      });

      expect(responseRole.role).toEqual(savedRole.role);

      savedRoles.push(savedRole);

      return data;
    });
    it('이미 등록되어있는 롤 등록 시 에러 반환', async () => {
      const { body } = await request(app.getHttpServer())
        .post(gqlEndpoint)
        .set('Authorization', `Bearer ${accessToken}`)
        .send({ query, variables })
        .expect(200);

      const error = body.errors[0].extensions.response;

      expect(error.message).toEqual(
        RolesExceptionMessage.DuplicateRole(variables.createRoleInput.role),
      );

      return body;
    });
  });
  describe('롤 삭제 (e2e)', () => {
    const query = `
    mutation RemoveRole($removeRoleInput: RemoveRoleInput!) {
      removeRole(removeRoleInput: $removeRoleInput) {
        remark
        useYn
        deletedBy
        deletedAt
      }
    }
    `;
    const variables = {
      removeRoleInput: {
        role: RoleTypes.TEST,
      },
    };
    it('롤 삭제', async () => {
      const data = await request(app.getHttpServer())
        .post(gqlEndpoint)
        .set('Authorization', `Bearer ${accessToken}`)
        .send({ query, variables })
        .expect(200);

      const savedRole = await prisma.role.findUnique({
        where: {
          role: variables.removeRoleInput.role,
        },
      });

      expect(savedRole).toBeNull();
      return data;
    });
    it('존재하지 않는 롤 삭제 시 에러 반환', async () => {});
  });
});
