import {
  CommonEntityBuilder,
  CommonValidateFunction,
  IJwUserRequest,
} from '@mion/toy-estate-lib';
import { Field, InputType, PickType } from '@nestjs/graphql';
import { Prisma } from '@prisma/client';
import { IsNumber } from 'class-validator';
import { JwtPayload } from '../../auth/auth.service';
import { CreateUserInput } from './create-user.input';

@InputType()
export class EditUserInput
  extends PickType(CreateUserInput, ['name'])
  implements CommonEntityBuilder<Prisma.UserUpdateArgs>, IJwUserRequest
{
  requestUser: JwtPayload;

  @Field((type) => Number, { description: '사용자 ID(PK)' })
  @IsNumber()
  userId: number;

  async build(): Promise<Prisma.UserUpdateArgs> {
    const model: Prisma.UserUpdateArgs = {
      data: {
        name: this.name,
        updatedBy: this.requestUser.email,
      },
      where: {
        id: this.requestUser.userId,
      },
    };
    return model;
  }
  async validate(validateFunction: CommonValidateFunction<any>): Promise<this> {
    await validateFunction.validate(this);
    return this;
  }
}
