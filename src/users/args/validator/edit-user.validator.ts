import { CommonValidateFunction } from '@mion/toy-estate-lib';
import { BadRequestException, Injectable } from '@nestjs/common';
import { RoleTypes } from '@prisma/client';
import { UserExceptionMessage } from '../../exceptions/users.exception.message';
import { EditUserInput } from '../edit-user.input';
import { UsersCommonValidator } from './common/users-common.validator';

@Injectable()
export class EditUserValidator
  extends UsersCommonValidator
  implements CommonValidateFunction<EditUserInput>
{
  async validate(dto: EditUserInput): Promise<void> {
    const savedUser = await super.validateUserIdExists(dto.userId);

    // role이 USER일 경우, 사용자 ID 일치 여부 확인
    const isUserRole = dto?.requestUser?.roles.find(
      (role) => role === RoleTypes.USER,
    );

    const isDifferentUser = savedUser.email !== dto.requestUser.email;

    if (isUserRole && isDifferentUser) {
      throw new BadRequestException(
        UserExceptionMessage.NotMatchUserId(
          savedUser.email,
          dto.requestUser.email,
        ),
      );
    }
  }
}
