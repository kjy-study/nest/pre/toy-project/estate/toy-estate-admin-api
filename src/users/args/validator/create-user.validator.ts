import { CommonValidateFunction } from '@mion/toy-estate-lib';
import { BadRequestException, Injectable } from '@nestjs/common';
import { BooleanTypes, User } from '@prisma/client';
import { UserExceptionMessage } from '../../exceptions/users.exception.message';
import { CreateUserInput } from '../create-user.input';
import { UsersCommonValidator } from './common/users-common.validator';

@Injectable()
export class CreateUserValidator
  extends UsersCommonValidator
  implements CommonValidateFunction<CreateUserInput>
{
  async validate(dto: CreateUserInput): Promise<User> {
    const savedUser = await this.usersRepository.findFirst({
      where: { email: dto.email, useYn: BooleanTypes.Y },
    });

    if (savedUser) {
      throw new BadRequestException(
        UserExceptionMessage.DuplicateEmail(dto.email),
      );
    }

    return savedUser;
  }
}
