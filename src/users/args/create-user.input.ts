import {
  BcryptHelper,
  CommonEntityBuilder,
  CommonValidateFunction,
} from '@mion/toy-estate-lib';
import { Field, InputType } from '@nestjs/graphql';
import { Prisma, RoleTypes } from '@prisma/client';
import { IsEmail, IsString, Matches } from 'class-validator';
import { UserExceptionMessage } from '../exceptions/users.exception.message';

@InputType()
export class CreateUserInput
  implements CommonEntityBuilder<Prisma.UserCreateArgs>
{
  @Field((type) => String, { description: '이메일' })
  @IsEmail({ message: '이메일은 문자로 입력해 주십시오.' })
  email: string;

  @Field((type) => String, { description: '사용자 이름' })
  @IsString({ message: '이름은 문자로 입력해 주십시오.' })
  name: string;

  @Field((type) => String, { description: '사용자 비밀번호' })
  @IsString({ message: '비밀번호는 문자로 입력해 주십시오.' })
  @Matches(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/, {
    message: UserExceptionMessage.InvalidPasswordForm(),
  })
  password: string;

  async build(): Promise<Prisma.UserCreateArgs> {
    const model: Prisma.UserCreateArgs = {
      data: {
        email: this.email,
        name: this.name,
        password: await BcryptHelper.hash(this.password),
        role: {
          create: {
            roleId: RoleTypes.USER,
            createdBy: this.email,
            updatedBy: this.email,
          },
        },
        createdBy: this.email,
        updatedBy: this.email,
      },
    };

    return model;
  }

  async validate(validateFunction: CommonValidateFunction<any>): Promise<this> {
    await validateFunction.validate(this);
    return this;
  }
}
