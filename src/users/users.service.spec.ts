import { PrismaService } from '@mion/toy-estate-lib';
import { BadRequestException, NotFoundException } from '@nestjs/common';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { Test, TestingModule } from '@nestjs/testing';
import { BooleanTypes, RoleTypes, User } from '@prisma/client';
import { AppModule } from '../app.module';
import { CreateUserInput } from './args/create-user.input';
import { EditUserInput } from './args/edit-user.input';
import { UserExceptionMessage } from './exceptions/users.exception.message';
import { UsersRepository } from './users.repository';
import { UsersService } from './users.service';

jest.setTimeout(60 * 1000);

describe('UsersService', () => {
  let service: UsersService;
  let repository: UsersRepository;
  let prismaService: PrismaService;

  let savedUser: User;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule, EventEmitterModule.forRoot()],
    }).compile();

    service = module.get<UsersService>(UsersService);
    repository = module.get<UsersRepository>(UsersRepository);
    prismaService = module.get<PrismaService>(PrismaService);
  });

  afterAll(async () => {
    if (savedUser?.id) {
      await prismaService.user.delete({
        where: {
          id: savedUser.id,
        },
      });
    }
  });

  describe('등록 테스트', () => {
    const reqCreateUser = {
      email: `${Math.random()}mion@gmail.com`,
      name: 'mion',
      password: 'password1234!',
      createdBy: 'mion@gmail.com',
    };

    it('정상 등록', async () => {
      const req: CreateUserInput = Object.assign(
        new CreateUserInput(),
        reqCreateUser,
      );

      savedUser = await service.create({ dto: req });

      expect(savedUser.id).toBeDefined();
    });

    it('중복된 이메일로 등록 시 예외 발생', async () => {
      const req: CreateUserInput = Object.assign(
        new CreateUserInput(),
        reqCreateUser,
      );

      const result = async () => {
        await service.create({ dto: req });
      };

      await expect(result()).rejects.toThrowError(
        new BadRequestException(UserExceptionMessage.DuplicateEmail(req.email)),
      );
    });
  });

  describe('수정 테스트', () => {
    const reqEditUser = {
      email: 'mion@gmail.com',
      name: '테스트코드에서 수정한 이름',
      password: 'password1234!',
      updatedBy: 'mion@gmail.com',
      roles: [RoleTypes.USER],
    };

    it('존재하지 않는 사용자 정보 수정 요청 시 에러 반환', async () => {
      const userId = 123123123312;
      const req: EditUserInput = Object.assign(new EditUserInput(), {
        userId: userId,
      });

      const result = async () => {
        await service.edit({ dto: req });
      };

      await expect(result()).rejects.toThrowError(
        new NotFoundException(UserExceptionMessage.NotExistsUser(userId)),
      );
    });

    it('다른 사용자가 정보 수정 요청 시 에러 반환', async () => {
      const userId = 130;
      const req: EditUserInput = Object.assign(new EditUserInput(), {
        requestUser: {
          userId: userId,
          email: 'mion2@gmail.com',
          roles: [RoleTypes.USER],
        },
      });

      const result = async () => {
        await service.edit({ dto: req });
      };

      const savedUser = await repository.findFirst({
        where: { id: userId, useYn: BooleanTypes.Y },
      });

      await expect(result()).rejects.toThrowError(
        new BadRequestException(
          UserExceptionMessage.NotMatchUserId(
            savedUser.email,
            req.requestUser.email,
          ),
        ),
      );
    });

    it('정상 수정', async () => {
      const userId = savedUser.id ?? 57;
      const req: EditUserInput = Object.assign(new EditUserInput(), {
        requestUser: {
          userId: savedUser.id,
          email: savedUser.email,
          roles: [RoleTypes.USER],
        },
      });

      const asIs = await repository.findFirst({
        where: {
          id: userId,
        },
      });

      req.userId = req.requestUser.userId;
      req.requestUser.email = asIs.email;

      await service.edit({ dto: req });
      const toBe = await repository.findFirst({
        where: {
          id: userId,
        },
      });

      const expectData: User = {
        id: userId,
        email: asIs.email,
        name: toBe.name,
        password: asIs.password,
        createdAt: asIs.createdAt,
        createdBy: asIs.createdBy,
        updatedAt: toBe.updatedAt,
        updatedBy: asIs.updatedBy,
        deletedAt: null,
        deletedBy: null,
        remark: null,
        useYn: BooleanTypes.Y,
      };

      expect(expectData).toEqual(toBe);
    });
  });
});

function delay(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}
