import { JwtUser } from '@mion/toy-estate-lib';
import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { RoleTypes } from '@prisma/client';
import { JwtPayload } from '../auth/auth.service';
import { Public } from '../auth/meta/public.meta';
import { Roles } from '../auth/meta/roles.meta';
import { CreateUserInput } from './args/create-user.input';
import { EditUserInput } from './args/edit-user.input';
import { UserModel } from './models/user.model';
import { UsersService } from './users.service';

@Resolver((of) => UserModel)
export class UsersResolver {
  constructor(private readonly usersService: UsersService) {}

  @Public()
  @Roles(RoleTypes.USER, RoleTypes.ADMIN)
  @Mutation((returns) => UserModel, { description: '회원가입' })
  async createUser(@Args('createUserInput') createUserInput: CreateUserInput) {
    const data = await this.usersService.create({ dto: createUserInput });
    return data;
  }

  @Roles(RoleTypes.USER, RoleTypes.ADMIN)
  @Mutation((returns) => UserModel, { description: '회원정보 수정' })
  async editUser(
    @Args('editUserInput') editUserInput: EditUserInput,
    @JwtUser() user: JwtPayload,
  ) {
    editUserInput.requestUser = user;

    const data = await this.usersService.edit({ dto: editUserInput });
    return data;
  }
}
