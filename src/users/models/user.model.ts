import { Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { BooleanTypes, User } from '@prisma/client';

@ObjectType()
export class UserModel implements User {
  @ApiProperty({
    example: 1,
    description: '사용자 ID(PK)',
  })
  @Field((type) => Number, { description: '사용자 ID(PK)' })
  id: number;

  @ApiProperty({
    example: 'mion@gmail.com',
    description: '이메일',
    nullable: false,
    required: true,
  })
  @Field((type) => String, { description: '이메일' })
  email: string;

  @ApiProperty({
    example: 'mion',
    description: '사용자 이름',
    nullable: true,
    required: true,
  })
  @Field((type) => String, { description: '사용자 이름' })
  name: string;

  @ApiProperty({
    example: 'password1234!',
    description: '사용자 비밀번호',
    nullable: false,
    required: true,
  })
  @Field((type) => String, { description: '사용자 비밀번호' })
  password: string;

  @ApiProperty({
    example: new Date(),
    description: '등록일시',
    enum: BooleanTypes,
    nullable: false,
    required: false,
  })
  @Field({ description: '등록일시' })
  createdAt: Date;

  @ApiProperty({
    example: 'mion',
    description: '등록자',
    nullable: false,
    required: true,
  })
  @Field((type) => String, { description: '등록자' })
  createdBy: string;

  @ApiProperty({
    example: new Date(),
    description: '수정일시',
    nullable: false,
    required: false,
  })
  @Field({ description: '수정일시' })
  updatedAt: Date;

  @ApiProperty({
    example: 'mion',
    description: '수정자',
    nullable: false,
    required: true,
  })
  @Field((type) => String, { description: '수정자' })
  updatedBy: string;

  @ApiProperty({
    example: new Date(),
    description: '삭제일시',
    nullable: true,
    required: false,
  })
  @Field({ description: '삭제일시' })
  deletedAt: Date;

  @ApiProperty({
    example: 'mion',
    description: '삭제자',
    nullable: true,
    required: true,
  })
  @Field((type) => String, { description: '삭제자' })
  deletedBy: string;

  @ApiProperty({
    example: BooleanTypes.Y,
    description: '사용여부',
    enum: BooleanTypes,
    nullable: false,
    required: false,
  })
  useYn: BooleanTypes;

  @ApiProperty({
    description: '비고',
    nullable: true,
    required: false,
  })
  remark: string;
}
