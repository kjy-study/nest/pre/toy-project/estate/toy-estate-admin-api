import { Module } from '@nestjs/common';
import { RolesModule } from '../roles/roles.module';
import { UsersCommonValidator } from './args/validator/common/users-common.validator';
import { CreateUserValidator } from './args/validator/create-user.validator';
import { EditUserValidator } from './args/validator/edit-user.validator';
import { UsersRepository } from './users.repository';
import { UsersService } from './users.service';
import { UsersResolver } from './users.resolver';
import { PrismaService } from '@mion/toy-estate-lib';

@Module({
  imports: [RolesModule],
  providers: [
    UsersService,
    UsersRepository,
    UsersCommonValidator,
    PrismaService,
    CreateUserValidator,
    EditUserValidator,
    UsersResolver,
  ],
  exports: [UsersCommonValidator, UsersRepository],
})
export class UsersModule {}
