import { Injectable } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { CreateUserInput } from './args/create-user.input';
import { EditUserInput } from './args/edit-user.input';
import { CreateUserValidator } from './args/validator/create-user.validator';
import { EditUserValidator } from './args/validator/edit-user.validator';
import { UsersRepository } from './users.repository';

@Injectable()
export class UsersService {
  constructor(
    private readonly usersRepository: UsersRepository,
    private readonly createUserValidator: CreateUserValidator,
    private readonly editUserValidator: EditUserValidator,
    private readonly eventEmitter: EventEmitter2,
  ) {}

  /**
   * 사용자 정보 생성
   * @param params
   * @returns
   */
  async create(params: { dto: CreateUserInput }) {
    const buildEntity = await (
      await params.dto.validate(this.createUserValidator)
    ).build();

    const savedData = await this.usersRepository.create(buildEntity);
    return savedData;
  }

  /**
   * 사용자 정보 수정
   * @param params
   * @returns
   */
  async edit(params: { dto: EditUserInput }) {
    const buildEntity = await (
      await params.dto.validate(this.editUserValidator)
    ).build();

    const editedData = await this.usersRepository.edit(buildEntity);

    return editedData;
  }
}
