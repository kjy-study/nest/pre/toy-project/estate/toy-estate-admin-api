import { Injectable } from '@nestjs/common';
import { Code } from '@prisma/client';
import { CodesRepository } from './codes.repository';
import { CreateCodeInput } from './args/create-code.input';
import { EditCodeInput } from './args/edit-code.input';
import { FindManyCodeArgs } from './args/find-many-code.args';
import { FindUniqueCodeArgs } from './args/find-unique-code.args';
import { RemoveCodeInput } from './args/remove-code.input';
import { CreateCodeValidator } from './args/validator/create-code.validator';
import { EditCodeValidator } from './args/validator/edit-code.validator';
import { RemoveCodeValidator } from './args/validator/remove-code.validator';
import { FindManyResponse } from '@mion/toy-estate-lib';

@Injectable()
export class CodesService {
  constructor(
    private readonly codeRepository: CodesRepository,
    private readonly createCodeValidator: CreateCodeValidator,
    private readonly editCodeValidator: EditCodeValidator,
    private readonly removeCodeValidator: RemoveCodeValidator,
  ) {}

  /**
   * 코드 정보 추가
   * @param createCodeDto
   */
  async create(params: { dto: CreateCodeInput }): Promise<Code> {
    await params.dto.validate(this.createCodeValidator);
    const buildEntity = await params.dto.build();
    const savedData = await this.codeRepository.create(buildEntity);

    return savedData;
  }

  /**
   * 코드 정보 목록 조회
   */
  async findMany(params: { dto: FindManyCodeArgs }): Promise<FindManyResponse> {
    const { findManyArgs, countManyArgs } = await params.dto.build();
    const savedData = await this.codeRepository.findMany(findManyArgs);
    const count = await this.codeRepository.count(countManyArgs);

    return { datas: savedData, count };
  }

  /**
   * 코드 정보 상세 조회
   * @param code
   */
  async findUnique(params: { dto: FindUniqueCodeArgs }) {
    const entityBuilder = await params.dto.build();

    const savedData = await this.codeRepository.findUnique(entityBuilder);
    return savedData;
  }

  /**
   * 코드 정보 수정
   * @param id
   * @param updateCodeDto
   */
  async edit(parmas: { dto: EditCodeInput }) {
    await parmas.dto.validate(this.editCodeValidator);
    const entityBuilder = await parmas.dto.build();
    const editedData = await this.codeRepository.edit(entityBuilder);

    return editedData;
  }

  /**
   * 코드 정보 삭제
   * @param removeCodeDto
   */
  async remove(params: { dto: RemoveCodeInput }) {
    await params.dto.validate(this.removeCodeValidator);
    const entityBuilder = await params.dto.build();

    const removedData = await this.codeRepository.softRemove(entityBuilder);

    return removedData;
  }
}
