import {
  CommonEntityBuilder,
  CommonFilterArgs,
  CommonValidateFunction,
  FindManyBuilderType,
} from '@mion/toy-estate-lib';
import { ArgsType, Field } from '@nestjs/graphql';
import { Prisma } from '@prisma/client';
import { IsString } from 'class-validator';

@ArgsType()
export class FindManyCodeArgs
  extends CommonFilterArgs
  implements
    CommonEntityBuilder<
      FindManyBuilderType<Prisma.CodeFindManyArgs, Prisma.CodeCountArgs>
    >
{
  @Field((type) => String, { description: '코드 그룹' })
  @IsString()
  codeGroup: string;

  async build(): Promise<
    FindManyBuilderType<Prisma.CodeFindManyArgs, Prisma.CodeCountArgs>
  > {
    const findManyArgs: Prisma.CodeFindManyArgs = {
      where: {
        codeGroup: this.codeGroup,
      },
      skip: this.page * this.size,
      take: this.size,
    };

    const countArgs: Prisma.CodeCountArgs = Object.assign(findManyArgs);

    return {
      findManyArgs: findManyArgs,
      countManyArgs: countArgs,
    };
  }

  // 목록 조회는 유효성 검사는 하지 않습니다.
  async validate(validateFunction: CommonValidateFunction<any>): Promise<this> {
    return this;
  }
}
