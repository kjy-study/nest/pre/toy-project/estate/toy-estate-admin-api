import {
  CommonEntityBuilder,
  CommonValidateFunction,
  dbNow,
  IJwUserRequest,
} from '@mion/toy-estate-lib';
import { InputType, PickType } from '@nestjs/graphql';
import { BooleanTypes, Prisma } from '@prisma/client';
import { JwtPayload } from '../../auth/auth.service';
import { CreateCodeInput } from './create-code.input';

@InputType()
export class RemoveCodeInput
  extends PickType(CreateCodeInput, ['codeGroup', 'code'])
  implements CommonEntityBuilder<Prisma.CodeUpdateArgs>, IJwUserRequest
{
  requestUser: JwtPayload;

  async build(): Promise<Prisma.CodeUpdateArgs> {
    const model: Prisma.CodeUpdateArgs = {
      data: {
        useYn: BooleanTypes.N,
        deletedBy: this.requestUser.email,
        deletedAt: dbNow(),
      },
      where: {
        codeGroup_code: {
          code: this.code,
          codeGroup: this.codeGroup,
        },
      },
    };

    return model;
  }

  async validate(validateFunction: CommonValidateFunction<any>): Promise<this> {
    await validateFunction.validate(this);
    return this;
  }
}
