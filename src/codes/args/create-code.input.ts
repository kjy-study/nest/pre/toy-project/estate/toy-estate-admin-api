import {
  CommonEntityBuilder,
  CommonValidateFunction,
  IJwUserRequest,
} from '@mion/toy-estate-lib';
import { Field, InputType } from '@nestjs/graphql';
import { Prisma } from '@prisma/client';
import { IsString } from 'class-validator';
import { JwtPayload } from '../../auth/auth.service';

@InputType()
export class CreateCodeInput
  implements CommonEntityBuilder<Prisma.CodeCreateArgs>, IJwUserRequest
{
  requestUser: JwtPayload;

  @Field((type) => String, { description: '코드 그룹' })
  @IsString()
  codeGroup: string;

  @Field((type) => String, { description: '코드' })
  @IsString()
  code: string;

  @Field((type) => String, { description: '코드명' })
  @IsString()
  codeNm: string;

  async build(): Promise<Prisma.CodeCreateArgs> {
    const model: Prisma.CodeCreateArgs = {
      data: {
        code: this.code,
        codeGroup: this.codeGroup,
        codeNm: this.codeNm,
        createdBy: this.requestUser.email,
        updatedBy: this.requestUser.email,
      },
    };
    return model;
  }
  async validate(validateFunction: CommonValidateFunction<any>): Promise<this> {
    await validateFunction.validate(this);
    return this;
  }
}
