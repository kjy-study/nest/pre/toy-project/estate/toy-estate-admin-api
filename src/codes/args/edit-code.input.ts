import {
  CommonEntityBuilder,
  CommonValidateFunction,
  IJwUserRequest,
} from '@mion/toy-estate-lib';
import { InputType, PickType } from '@nestjs/graphql';
import { Prisma } from '@prisma/client';
import { JwtPayload } from '../../auth/auth.service';
import { CreateCodeInput } from './create-code.input';

@InputType()
export class EditCodeInput
  extends PickType(CreateCodeInput, ['codeGroup', 'code', 'codeNm'])
  implements CommonEntityBuilder<Prisma.CodeUpdateArgs>, IJwUserRequest
{
  requestUser: JwtPayload;

  async build(params?: any): Promise<Prisma.CodeUpdateArgs> {
    return {
      data: {
        codeNm: this.codeNm,
        updatedBy: this.requestUser.email,
      },
      where: {
        codeGroup_code: {
          codeGroup: this.codeGroup,
          code: this.code,
        },
      },
    };
  }

  async validate(
    validateFunction: CommonValidateFunction<any>,
    parmas?: any,
  ): Promise<this> {
    await validateFunction.validate(this);
    return this;
  }
}
