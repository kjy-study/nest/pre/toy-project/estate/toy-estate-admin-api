import {
  CommonEntityBuilder,
  CommonValidateFunction,
} from '@mion/toy-estate-lib';
import { ArgsType, Field } from '@nestjs/graphql';
import { Prisma } from '@prisma/client';
import { IsString } from 'class-validator';

@ArgsType()
export class FindUniqueCodeArgs
  implements CommonEntityBuilder<Prisma.CodeFindUniqueArgs>
{
  @Field((type) => String, { description: '코드 그룹' })
  @IsString()
  codeGroup: string;

  @Field((type) => String, { description: '코드' })
  @IsString()
  code: string;

  async build(): Promise<Prisma.CodeFindUniqueArgs> {
    const model: Prisma.CodeFindUniqueArgs = {
      where: {
        codeGroup_code: {
          codeGroup: this.codeGroup,
          code: this.code,
        },
      },
    };
    return model;
  }
  async validate(validateFunction: CommonValidateFunction<any>): Promise<this> {
    return this;
  }
}
