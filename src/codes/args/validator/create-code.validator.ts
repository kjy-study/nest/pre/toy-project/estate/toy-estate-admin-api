import { CommonValidateFunction } from '@mion/toy-estate-lib';
import { ConflictException, Injectable } from '@nestjs/common';
import { CodesExceptionMessage } from '../../exceptions/codes.exception.message';
import { CreateCodeInput } from '../create-code.input';
import { CodesCommonValidator } from './common/codes-common.validator';

@Injectable()
export class CreateCodeValidator
  extends CodesCommonValidator
  implements CommonValidateFunction<CreateCodeInput>
{
  async validate(dto: CreateCodeInput): Promise<void> {
    const savedCode = await this.codesRepository.findUnique({
      where: {
        codeGroup_code: {
          codeGroup: dto.codeGroup,
          code: dto.code,
        },
      },
    });

    if (savedCode) {
      throw new ConflictException(
        CodesExceptionMessage.DuplicateCode(dto.codeGroup, dto.code),
      );
    }
  }
}
