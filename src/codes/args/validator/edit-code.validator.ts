import { CommonValidateFunction } from '@mion/toy-estate-lib';
import { Injectable } from '@nestjs/common';
import { EditCodeInput } from '../edit-code.input';
import { CodesCommonValidator } from './common/codes-common.validator';

@Injectable()
export class EditCodeValidator
  extends CodesCommonValidator
  implements CommonValidateFunction<EditCodeInput>
{
  async validate(dto: EditCodeInput): Promise<void> {
    await super.validateCodeGroupExists({
      codeGroup: dto.codeGroup,
    });

    await super.validateCodeExists({
      codeGroup: dto.codeGroup,
      code: dto.code,
    });
  }
}
