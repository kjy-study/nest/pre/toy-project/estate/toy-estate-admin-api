import { CommonValidateFunction } from '@mion/toy-estate-lib';
import { Injectable } from '@nestjs/common';
import { RemoveCodeInput } from '../remove-code.input';
import { CodesCommonValidator } from './common/codes-common.validator';

@Injectable()
export class RemoveCodeValidator
  extends CodesCommonValidator
  implements CommonValidateFunction<RemoveCodeInput>
{
  async validate(dto: RemoveCodeInput): Promise<void> {
    await super.validateCodeExists({
      codeGroup: dto.codeGroup,
      code: dto.code,
    });
  }
}
