import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../app.module';
import { CodesResolver } from './codes.resolver';

describe('CodesResolver', () => {
  let resolver: CodesResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    resolver = module.get<CodesResolver>(CodesResolver);
  });

  it('should be defined', () => {});
});
