import { Args, Mutation, Query, Resolver, Subscription } from '@nestjs/graphql';
import { RoleTypes } from '@prisma/client';
import { PubSub } from 'graphql-subscriptions';
import { JwtPayload } from '../auth/auth.service';
import { Public } from '../auth/meta/public.meta';
import { Roles } from '../auth/meta/roles.meta';
import { CodesService } from './codes.service';
import { CreateCodeInput } from './args/create-code.input';
import { EditCodeInput } from './args/edit-code.input';
import { FindManyCodeArgs } from './args/find-many-code.args';
import { FindUniqueCodeArgs } from './args/find-unique-code.args';
import { RemoveCodeInput } from './args/remove-code.input';
import { CodeListModel } from './models/code.list-model';
import { CodeModel } from './models/code.model';
import { IListFormType, JwtUser } from '@mion/toy-estate-lib';

const pubSub = new PubSub();

@Resolver((of) => CodeModel)
export class CodesResolver {
  constructor(private readonly codeService: CodesService) {}

  @Roles(RoleTypes.ADMIN, RoleTypes.SYSTEM)
  @Query((returns) => CodeListModel, {
    description: '코드 목록 조회',
  })
  async codes(
    @Args() findManyCodeArgs: FindManyCodeArgs,
  ): Promise<IListFormType<CodeModel>> {
    const { datas, count } = await this.codeService.findMany({
      dto: findManyCodeArgs,
    });
    return {
      items: datas,
      total: count,
    };
  }

  @Roles(RoleTypes.ADMIN, RoleTypes.SYSTEM)
  @Query((returns) => CodeModel, {
    description: '코드 상세 조회',
    nullable: true,
  })
  async code(
    @Args() findUniqueCodeArgs: FindUniqueCodeArgs,
  ): Promise<CodeModel> {
    const data = await this.codeService.findUnique({ dto: findUniqueCodeArgs });
    return data;
  }

  @Roles(RoleTypes.ADMIN, RoleTypes.SYSTEM)
  @Mutation((returns) => CodeModel, { description: '코드 등록' })
  async createCode(
    @Args('createCodeInput') createCodeInput: CreateCodeInput,
    @JwtUser() user: JwtPayload,
  ) {
    createCodeInput.requestUser = user;
    const savedData = await this.codeService.create({ dto: createCodeInput });

    pubSub.publish('createdCode', { createdCode: savedData });
    return savedData;
  }

  @Roles(RoleTypes.ADMIN, RoleTypes.SYSTEM)
  @Mutation((returns) => CodeModel, { description: '코드 수정' })
  async editCode(
    @Args('editCodeInput') editCodeInput: EditCodeInput,
    @JwtUser() user: JwtPayload,
  ) {
    editCodeInput.requestUser = user;
    const editedData = await this.codeService.edit({ dto: editCodeInput });

    return editedData;
  }

  @Roles(RoleTypes.ADMIN, RoleTypes.SYSTEM)
  @Mutation((returns) => CodeModel, { description: '코드 삭제' })
  async removeCode(
    @Args('removeCodeInput') removeCodeInput: RemoveCodeInput,
    @JwtUser() user: JwtPayload,
  ) {
    removeCodeInput.requestUser = user;
    const editedData = await this.codeService.remove({ dto: removeCodeInput });

    return editedData;
  }

  @Public()
  @Subscription((returns) => CodeModel, { description: '코드 등록 이벤트' })
  createdCode() {
    const sub = pubSub.asyncIterator(['createdCode']);
    return sub;
  }
}
