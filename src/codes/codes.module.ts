import { Module } from '@nestjs/common';
import { CodesRepository } from './codes.repository';
import { CodesResolver } from './codes.resolver';
import { CodesService } from './codes.service';
import { CreateCodeValidator } from './args/validator/create-code.validator';
import { EditCodeValidator } from './args/validator/edit-code.validator';
import { RemoveCodeValidator } from './args/validator/remove-code.validator';
import { PrismaService, DateScalar } from '@mion/toy-estate-lib';

@Module({
  providers: [
    CodesService,
    CodesRepository,
    PrismaService,
    CreateCodeValidator,
    EditCodeValidator,
    RemoveCodeValidator,

    // graphql
    CodesResolver,
    DateScalar,
  ],
})
export class CodesModule {}
