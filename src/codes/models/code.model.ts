import { Directive, Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { BooleanTypes, Code } from '@prisma/client';

@ObjectType({ description: 'codes' })
export class CodeModel implements Code {
  @ApiProperty({
    example: 'TEST',
    description: '코드 그룹 (PK)',
    nullable: false,
    required: true,
  })
  @Field((type) => String)
  codeGroup: string;

  @ApiProperty({
    example: 'CODE_001',
    description: '코드 (PK)',
    nullable: false,
    required: true,
  })
  @Field((type) => String)
  code: string;

  @ApiProperty({
    example: '코드001',
    description: '코드 이름',
    nullable: false,
    required: true,
  })
  @Directive('@upper') // 필드의 값을 대문자로 변경합니다.
  @Field((type) => String)
  codeNm: string;

  @ApiProperty({
    example: new Date(),
    description: '등록일시',
    enum: BooleanTypes,
    nullable: false,
    required: false,
  })
  @Field()
  createdAt: Date;

  @ApiProperty({
    example: 'mion',
    description: '등록자',
    nullable: false,
    required: true,
  })
  @Field((type) => String)
  createdBy: string;

  @ApiProperty({
    example: new Date(),
    description: '수정일시',
    nullable: false,
    required: false,
  })
  @Field()
  updatedAt: Date;

  @ApiProperty({
    example: 'mion',
    description: '수정자',
    nullable: false,
    required: true,
  })
  @Field((type) => String)
  updatedBy: string;

  @ApiProperty({
    example: null,
    description: '삭제일시',
    nullable: true,
    required: false,
  })
  @Field()
  deletedAt: Date;

  @ApiProperty({
    example: null,
    description: '삭제자',
    nullable: true,
    required: true,
  })
  @Field((type) => String, { nullable: true })
  deletedBy: string;

  @ApiProperty({
    example: BooleanTypes.Y,
    description: '사용여부',
    enum: BooleanTypes,
    nullable: false,
    required: false,
  })
  @Field((type) => BooleanTypes)
  useYn: BooleanTypes;

  @ApiProperty({
    example: '',
    description: '비고',
    nullable: true,
    required: false,
  })
  @Field((type) => String, { nullable: true })
  remark: string;
}
