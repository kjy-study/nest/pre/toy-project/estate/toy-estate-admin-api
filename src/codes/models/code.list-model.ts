import { ListGraphQLResponseForm } from '@mion/toy-estate-lib';
import { ObjectType } from '@nestjs/graphql';
import { CodeModel } from './code.model';

@ObjectType()
export class CodeListModel extends ListGraphQLResponseForm(CodeModel) {}
