import { upperDirectiveTransformer } from '@mion/toy-estate-lib';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { GraphQLModule } from '@nestjs/graphql';
import { RoleTypes } from '@prisma/client';
import { ApolloServerPluginLandingPageLocalDefault } from 'apollo-server-core';
import { DirectiveLocation, GraphQLDirective } from 'graphql';
import { join } from 'path';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { RoleGuard } from './auth/guards/role.guard';
import { CodesModule } from './codes/codes.module';
import { configuration } from './config/configuration';
import { RolesModule } from './roles/roles.module';
import { UsersModule } from './users/users.module';

@Module({
  imports: [
    // config service
    ConfigModule.forRoot({
      load: [configuration],
      isGlobal: true,
    }),

    // event emitter
    EventEmitterModule.forRoot(),

    // GraphQL
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
      sortSchema: true,
      transformSchema: (schema) => upperDirectiveTransformer(schema, 'upper'),
      installSubscriptionHandlers: true, // subscription 활성화(추후 삭제될 옵션)
      // subscriptions: {
      //   'graphql-ws': true,
      // },
      buildSchemaOptions: {
        directives: [
          new GraphQLDirective({
            name: 'upper',
            locations: [DirectiveLocation.FIELD_DEFINITION],
          }),
        ],
      },
      playground: false,
      plugins: [ApolloServerPluginLandingPageLocalDefault()],
      stopOnTerminationSignals: true,
      resolvers: [{ RoleTypes }],
    }),

    AuthModule,
    CodesModule,
    UsersModule,
    RolesModule,
  ],

  controllers: [AppController],
  providers: [AppService, RoleGuard],
})
export class AppModule {}
