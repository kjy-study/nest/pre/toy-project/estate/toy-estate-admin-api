import { PrismaService } from '@mion/toy-estate-lib';
import { ConflictException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { RoleTypes } from '@prisma/client';
import { AppModule } from '../app.module';
import { CreateRoleInput } from './args/create-role.input';
import { RolesExceptionMessage } from './exceptions/roles.exception.message';
import { RoleModel } from './models/role.model';
import { RolesRepository } from './roles.repository';
import { RolesService } from './roles.service';

describe('RolesService', () => {
  let service: RolesService;
  let repository: RolesRepository;
  let prismaService: PrismaService;
  let savedData: RoleModel;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    service = module.get<RolesService>(RolesService);
    repository = module.get<RolesRepository>(RolesRepository);
    prismaService = module.get<PrismaService>(PrismaService);
  });

  afterAll(async () => {
    if (savedData?.role) {
      await prismaService.role.delete({
        where: {
          role: savedData.role,
        },
      });
    }
  });

  describe('등록 테스트', () => {
    const reqCreateRole = {
      role: RoleTypes.TEST,
      requestUser: {
        userId: 130,
        email: 'mion@gmail.com',
        roles: ['USER'],
      },
    };

    it('정상 등록', async () => {
      // 중복 데이터
      const input: CreateRoleInput = Object.assign(
        new CreateRoleInput(),
        reqCreateRole,
      );

      savedData = await service.create({ args: input });
    });

    it('롤 중복 예외 발생', async () => {
      // 중복 데이터
      const input: CreateRoleInput = Object.assign(
        new CreateRoleInput(),
        reqCreateRole,
      );

      const result = async () => {
        await service.create({ args: input });
      };

      await expect(result()).rejects.toThrowError(
        new ConflictException(RolesExceptionMessage.DuplicateRole(input.role)),
      );
    });
  });
});
