import {
  CommonEntityBuilder,
  CommonValidateFunction,
  IJwUserRequest,
} from '@mion/toy-estate-lib';
import { Field, InputType } from '@nestjs/graphql';
import { Prisma, RoleTypes } from '@prisma/client';
import { IsEnum } from 'class-validator';
import { JwtPayload } from '../../auth/auth.service';

@InputType()
export class CreateRoleInput
  implements CommonEntityBuilder<Prisma.RoleCreateArgs>, IJwUserRequest
{
  requestUser: JwtPayload;

  @Field((type) => RoleTypes, { description: '권한 타입' })
  @IsEnum(RoleTypes)
  role: RoleTypes;

  async build(params?: any): Promise<Prisma.RoleCreateArgs> {
    const model: Prisma.RoleCreateArgs = {
      data: {
        role: this.role,
        createdBy: this.requestUser.email,
        updatedBy: this.requestUser.email,
      },
    };

    return model;
  }

  async validate(
    validateFunction: CommonValidateFunction<any>,
    parmas?: any,
  ): Promise<this> {
    await validateFunction.validate(this);
    return this;
  }
}
