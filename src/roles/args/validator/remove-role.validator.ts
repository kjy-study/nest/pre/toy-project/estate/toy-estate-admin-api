import { CommonValidateFunction } from '@mion/toy-estate-lib';
import { Injectable } from '@nestjs/common';
import { RemoveRoleInput } from '../remove-role.input';
import { RolesCommonValidator } from './common/roles-common.validator';

@Injectable()
export class RemoveRoleValidator
  extends RolesCommonValidator
  implements CommonValidateFunction<RemoveRoleInput>
{
  async validate(input: RemoveRoleInput): Promise<void> {
    await super.validateRoleExists({ role: input.role });
  }
}
