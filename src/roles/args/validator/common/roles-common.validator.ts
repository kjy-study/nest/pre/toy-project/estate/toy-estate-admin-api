import { Injectable, NotFoundException } from '@nestjs/common';
import { RoleTypes } from '@prisma/client';
import { RolesExceptionMessage } from '../../../exceptions/roles.exception.message';
import { RolesRepository } from '../../../roles.repository';

@Injectable()
export class RolesCommonValidator {
  constructor(protected readonly rolesRepository: RolesRepository) {}

  /**
   * 롤이 존재하는지 유효성 검사
   * @param params
   * @returns
   */
  async validateRoleExists(params: { role: RoleTypes }) {
    const savedRole = await this.rolesRepository.findUnique({
      where: {
        role: params.role,
      },
    });

    if (!savedRole) {
      throw new NotFoundException(
        RolesExceptionMessage.NotExistsRole(params.role),
      );
    }

    return savedRole;
  }
}
