import { CommonValidateFunction } from '@mion/toy-estate-lib';
import { ConflictException, Injectable } from '@nestjs/common';
import { RolesExceptionMessage } from '../../exceptions/roles.exception.message';
import { CreateRoleInput } from '../create-role.input';
import { RolesCommonValidator } from './common/roles-common.validator';

@Injectable()
export class CreateRoleValidator
  extends RolesCommonValidator
  implements CommonValidateFunction<CreateRoleInput>
{
  async validate(input: CreateRoleInput): Promise<void> {
    const savedRole = await this.rolesRepository.findUnique({
      where: {
        role: input.role,
      },
    });

    if (savedRole) {
      throw new ConflictException(
        RolesExceptionMessage.DuplicateRole(input.role),
      );
    }
  }
}
