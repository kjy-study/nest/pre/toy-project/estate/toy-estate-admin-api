import { CommonEntityBuilder, CommonValidateFunction } from '@mion/toy-estate-lib';
import { ArgsType, Field } from '@nestjs/graphql';
import { Prisma, RoleTypes } from '@prisma/client';
import { IsEnum } from 'class-validator';

@ArgsType()
export class FindUniqueRoleArgs
  implements CommonEntityBuilder<Prisma.RoleFindUniqueArgs>
{
  @Field((type) => RoleTypes, { description: '역할' })
  @IsEnum(RoleTypes)
  role: RoleTypes;

  async build(): Promise<Prisma.RoleFindUniqueArgs> {
    const args: Prisma.RoleFindUniqueArgs = {
      where: {
        role: this.role,
      },
    };

    return args;
  }

  // 상세 조회는 유효성 검사를 하지 않습니다.
  async validate(validateFunction: CommonValidateFunction<any>): Promise<this> {
    return this;
  }
}
