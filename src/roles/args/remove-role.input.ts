import {
  CommonEntityBuilder,
  CommonValidateFunction,
  IJwUserRequest,
} from '@mion/toy-estate-lib';
import { InputType, PickType } from '@nestjs/graphql';
import { Prisma } from '@prisma/client';
import { JwtPayload } from '../../auth/auth.service';
import { CreateRoleInput } from './create-role.input';

@InputType()
export class RemoveRoleInput
  extends PickType(CreateRoleInput, ['role'])
  implements CommonEntityBuilder<Prisma.RoleDeleteArgs>, IJwUserRequest
{
  requestUser: JwtPayload;

  async build(params?: any): Promise<Prisma.RoleDeleteArgs> {
    const model: Prisma.RoleDeleteArgs = {
      where: {
        role: this.role,
      },
    };

    return model;
  }

  async validate(
    validateFunction: CommonValidateFunction<any>,
    parmas?: any,
  ): Promise<this> {
    await validateFunction.validate(this);
    return this;
  }
}
