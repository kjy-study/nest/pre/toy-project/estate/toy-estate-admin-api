import {
  CommonEntityBuilder,
  CommonFilterArgs,
  CommonValidateFunction,
  FindManyBuilderType,
} from '@mion/toy-estate-lib';
import { ArgsType, Field } from '@nestjs/graphql';
import { Prisma, RoleTypes } from '@prisma/client';
import { IsEnum, IsOptional } from 'class-validator';

@ArgsType()
export class FindManyRoleArgs
  extends CommonFilterArgs
  implements
    CommonEntityBuilder<
      FindManyBuilderType<Prisma.RoleFindManyArgs, Prisma.RoleCountArgs>
    >
{
  @Field((type) => RoleTypes, { description: '역할', nullable: true })
  @IsEnum(RoleTypes)
  @IsOptional()
  role: RoleTypes;

  async build(): Promise<
    FindManyBuilderType<Prisma.RoleFindManyArgs, Prisma.RoleCountArgs>
  > {
    const findManyArgs: Prisma.RoleFindManyArgs = {
      where: {
        role: this.role,
      },
    };

    const countArgs: Prisma.RoleCountArgs = Object.assign(findManyArgs);

    return {
      findManyArgs: findManyArgs,
      countManyArgs: countArgs,
    };
  }

  // 목록 조회는 유효성 검사를 하지 않습니다.
  async validate(
    validateFunction: CommonValidateFunction<any>,
    parmas?: any,
  ): Promise<this> {
    return this;
  }
}
