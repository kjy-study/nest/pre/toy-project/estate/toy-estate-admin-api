import { RoleTypes } from '@prisma/client';

export class RolesExceptionMessage {
  static NotExistsRole(role: RoleTypes) {
    return `롤[${role}]이 존재하지 않습니다.`;
  }

  static DuplicateRole(role: RoleTypes) {
    return `롤[${role}]이 이미 존재합니다.`;
  }
}
