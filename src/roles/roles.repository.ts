import { CommonRepository, PrismaService } from '@mion/toy-estate-lib';
import { Injectable } from '@nestjs/common';
import { Prisma } from '@prisma/client';

@Injectable()
export class RolesRepository implements CommonRepository {
  constructor(private readonly prisma: PrismaService) {}
  async create(roleCreateArgs: Prisma.RoleCreateArgs) {
    return await this.prisma.role.create(roleCreateArgs);
  }
  async findFirst(findFirstArgs: Prisma.RoleFindFirstArgs): Promise<any> {
    return await this.prisma.role.findFirst(findFirstArgs);
  }
  async findUnique(findUniqueArgs: Prisma.RoleFindUniqueArgs) {
    return await this.prisma.role.findUnique(findUniqueArgs);
  }
  async findMany(findManyArgs: Prisma.RoleFindManyArgs) {
    return await this.prisma.role.findMany(findManyArgs);
  }
  async count(countArgs: Prisma.RoleCountArgs) {
    return await this.prisma.role.count(countArgs);
  }
  async edit(roleUpdateArgs: Prisma.RoleUpdateArgs) {
    return await this.prisma.role.update(roleUpdateArgs);
  }
  async softRemove(roleUpdateArgs: Prisma.RoleUpdateArgs) {
    return await this.prisma.role.update(roleUpdateArgs);
  }
  async remove(roleDeleteArgs: Prisma.RoleDeleteArgs) {
    return await this.prisma.role.delete(roleDeleteArgs);
  }
}
