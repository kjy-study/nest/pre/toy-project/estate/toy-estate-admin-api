import { FindManyResponse } from '@mion/toy-estate-lib';
import { Injectable } from '@nestjs/common';
import { CreateRoleInput } from './args/create-role.input';
import { FindManyRoleArgs } from './args/find-many-role.args';
import { FindUniqueRoleArgs } from './args/find-unique-role.args';
import { RemoveRoleInput } from './args/remove-role.input';
import { CreateRoleValidator } from './args/validator/create-role.validator';
import { RemoveRoleValidator } from './args/validator/remove-role.validator';
import { RolesRepository } from './roles.repository';

@Injectable()
export class RolesService {
  constructor(
    private readonly rolesRepository: RolesRepository,
    private readonly createRoleValidator: CreateRoleValidator,
    private readonly removeRoleValidator: RemoveRoleValidator,
  ) {}

  /**
   * 롤 정보 목록 조회
   * @param params
   * @returns
   */
  async findMany(params: {
    args: FindManyRoleArgs;
  }): Promise<FindManyResponse> {
    const { findManyArgs, countManyArgs } = await params.args.build();

    const savedData = await this.rolesRepository.findMany(findManyArgs);
    const count = await this.rolesRepository.count(countManyArgs);

    return { datas: savedData, count };
  }

  /**
   * 롤 상세 정보 조회
   * @param params
   */
  async findUnique(params: { args: FindUniqueRoleArgs }) {
    const entityBuilder = await params.args.build();

    const savedData = await this.rolesRepository.findUnique(entityBuilder);
    return savedData;
  }

  /**
   * 롤 정보 추가
   * @param params
   */
  async create(params: { args: CreateRoleInput }) {
    await params.args.validate(this.createRoleValidator);
    const buildEntity = await params.args.build();
    const savedData = await this.rolesRepository.create(buildEntity);

    return savedData;
  }

  /**
   * 롤 정보 삭제
   * @param params
   */
  async remove(params: { args: RemoveRoleInput }) {
    await params.args.validate(this.removeRoleValidator);

    const buildEntity = await params.args.build();
    const removedData = await this.rolesRepository.remove(buildEntity);

    return removedData;
  }
}
