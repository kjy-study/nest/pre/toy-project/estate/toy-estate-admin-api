import { PrismaService, DateScalar } from '@mion/toy-estate-lib';
import { Module } from '@nestjs/common';
import { CreateRoleValidator } from './args/validator/create-role.validator';
import { RemoveRoleValidator } from './args/validator/remove-role.validator';
import { RolesRepository } from './roles.repository';
import { RolesResolver } from './roles.resolver';
import { RolesService } from './roles.service';

@Module({
  providers: [
    RolesService,
    RolesRepository,
    PrismaService,
    CreateRoleValidator,
    RemoveRoleValidator,

    RolesResolver,
    DateScalar,
  ],
  exports: [RolesRepository],
})
export class RolesModule {}
