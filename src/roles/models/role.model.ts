import { Field, ObjectType } from '@nestjs/graphql';
import { BooleanTypes, Role, RoleTypes } from '@prisma/client';

@ObjectType({ description: 'roles' })
export class RoleModel implements Role {
  @Field((type) => RoleTypes)
  role: RoleTypes;

  @Field()
  createdAt: Date;

  @Field((type) => String)
  createdBy: string;

  @Field()
  updatedAt: Date;

  @Field((type) => String)
  updatedBy: string;

  @Field()
  deletedAt: Date;

  @Field((type) => String, { nullable: true })
  deletedBy: string;

  @Field((type) => BooleanTypes)
  useYn: BooleanTypes;

  @Field((type) => String, { nullable: true })
  remark: string;
}
