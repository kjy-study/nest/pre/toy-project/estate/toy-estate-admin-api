import { ListGraphQLResponseForm } from '@mion/toy-estate-lib';
import { ObjectType } from '@nestjs/graphql';
import { RoleModel } from './role.model';

@ObjectType({ description: 'roles' })
export class RoleListModel extends ListGraphQLResponseForm(RoleModel) {}
