import { IListFormType, JwtUser } from '@mion/toy-estate-lib';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { RoleTypes } from '@prisma/client';
import { JwtPayload } from '../auth/auth.service';
import { Roles } from '../auth/meta/roles.meta';
import { CreateRoleInput } from './args/create-role.input';
import { FindManyRoleArgs } from './args/find-many-role.args';
import { FindUniqueRoleArgs } from './args/find-unique-role.args';
import { RemoveRoleInput } from './args/remove-role.input';
import { RoleListModel } from './models/role.list-model';
import { RoleModel } from './models/role.model';
import { RolesService } from './roles.service';

@Resolver((of) => RoleModel)
export class RolesResolver {
  constructor(private readonly rolesService: RolesService) {}

  @Roles(RoleTypes.ADMIN, RoleTypes.SYSTEM)
  @Query((returns) => RoleListModel, {
    description: '롤 목록 조회',
  })
  async roles(
    @Args() findManyRoleArgs: FindManyRoleArgs,
  ): Promise<IListFormType<RoleModel>> {
    const { datas, count } = await this.rolesService.findMany({
      args: findManyRoleArgs,
    });
    return {
      items: datas,
      total: count,
    };
  }

  @Roles(RoleTypes.ADMIN, RoleTypes.SYSTEM)
  @Query((returns) => RoleModel, {
    description: '롤 상세 조회',
  })
  async role(
    @Args() findUniqueRoleArgs: FindUniqueRoleArgs,
  ): Promise<RoleModel> {
    const data = await this.rolesService.findUnique({
      args: findUniqueRoleArgs,
    });
    return data;
  }

  @Roles(RoleTypes.ADMIN, RoleTypes.SYSTEM)
  @Mutation((returns) => RoleModel, { description: '롤 등록' })
  async createRole(
    @Args('createRoleInput') createRoleInput: CreateRoleInput,
    @JwtUser() user: JwtPayload,
  ) {
    createRoleInput.requestUser = user;

    const savedData = await this.rolesService.create({ args: createRoleInput });
    return savedData;
  }

  @Roles(RoleTypes.ADMIN, RoleTypes.SYSTEM)
  @Mutation((returns) => RoleModel, { description: '롤 삭제' })
  async removeRole(
    @Args('removeRoleInput') removeRoleInput: RemoveRoleInput,
    @JwtUser() user: JwtPayload,
  ) {
    removeRoleInput.requestUser = user;

    const editedData = await this.rolesService.remove({
      args: removeRoleInput,
    });
    return editedData;
  }
}
