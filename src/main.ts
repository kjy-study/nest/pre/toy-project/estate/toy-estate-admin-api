import {
  AllExceptionsGraphqlFilter,
  PrismaService,
} from '@mion/toy-estate-lib';
import { ValidationPipe, VersioningType } from '@nestjs/common';
import { INestApplication } from '@nestjs/common/interfaces/nest-application.interface';
import { ConfigService } from '@nestjs/config';
import { HttpAdapterHost, NestFactory, Reflector } from '@nestjs/core';
import { registerEnumType } from '@nestjs/graphql';
import { ExpressAdapter } from '@nestjs/platform-express';
import { BooleanTypes, RoleTypes } from '@prisma/client';
import { createServer } from 'aws-serverless-express';
import { eventContext } from 'aws-serverless-express/middleware';
import { Server } from 'http';
import { AppModule } from './app.module';
import { JwtAuthGuard } from './auth/guards/jwt-auth.guard';
import { RoleGuard } from './auth/guards/role.guard';

export async function bootstrap() {
    const app = await NestFactory.create(AppModule)
    const configService = app.get(ConfigService);
    await setGlobalOption(app);

  await app.listen(configService.get<number>('application.port'))
}

bootstrap();

export async function setGlobalOption(app: INestApplication) {
  // URI Versioning
  app.enableVersioning({
    type: VersioningType.URI,
  });

  // pipe
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      transformOptions: {
        // 암묵적 타입 변환
        enableImplicitConversion: true,
      },
      whitelist: true,
      forbidNonWhitelisted: true,
    }),
  );

  // filter
  const httpAdapterHost = app.get(HttpAdapterHost);
  app.useGlobalFilters(new AllExceptionsGraphqlFilter());

  // guard
  const reflector = app.get(Reflector);
  const jwtAuthGuard = new JwtAuthGuard(reflector);
  const roleGuard = app.get(RoleGuard);
  app.useGlobalGuards(jwtAuthGuard, roleGuard);

  // prisma
  const prismaService = app.get(PrismaService);
  await prismaService.enableShutdownHooks(app);

  //graphql
  registerEnumType(BooleanTypes, { name: 'BooleanTypes' });
  registerEnumType(RoleTypes, { name: 'RoleTypes' });
}
