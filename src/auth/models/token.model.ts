import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType({ description: '토큰 발급 처리 반환 결과' })
export class TokenModel {
  @Field((type) => String, { description: 'Access Token' })
  accessToken: string;

  @Field((type) => String, { description: 'Refresh Token' })
  refreshToken: string;
}
