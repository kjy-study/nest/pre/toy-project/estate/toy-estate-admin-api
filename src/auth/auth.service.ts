import { PrismaService } from '@mion/toy-estate-lib';
import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { BooleanTypes, RoleTypes } from '@prisma/client';
import { UsersRepository } from '../users/users.repository';
import { LoginInput } from './args/login.input';
import { LoginValidator } from './args/validator/login.validator';
import { TokenModel } from './models/token.model';

export class JwtPayload {
  userId: number;
  email: string;
  name: string;
  roles: RoleTypes[];
}

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly usersRepository: UsersRepository,
    private readonly loginValidator: LoginValidator,
    private readonly prisma: PrismaService,
  ) {}

  /**
   * jwt 토큰 생성
   * @param user
   */
  async createToken(loginInput: LoginInput): Promise<TokenModel> {
    await loginInput.validate(this.loginValidator);

    const savedUser = await this.usersRepository.findFirst({
      where: {
        email: loginInput.email,
        useYn: BooleanTypes.Y,
      },
      include: {
        role: true,
      },
    });

    const jwtPayload = new JwtPayload();
    jwtPayload.userId = savedUser.id;
    jwtPayload.email = savedUser.email;
    jwtPayload.name = savedUser.name;
    jwtPayload.roles = savedUser?.role?.map((roleData) => roleData.roleId);

    return {
      accessToken: this.jwtService.sign(Object.assign({}, jwtPayload)),
      refreshToken: this.jwtService.sign(Object.assign({}, jwtPayload), {
        secret: process.env.JWT_REFESH_SECRET,
        expiresIn: '7d',
      }),
    };
  }
}
