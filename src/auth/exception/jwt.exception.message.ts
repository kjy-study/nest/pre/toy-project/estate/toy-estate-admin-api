import dayjs from 'dayjs';

export class JwtExceptionMessage {
  static MalformedToken(): any {
    return `Authorization - Bearer 토큰이 유효하지 않은 형식 입니다.`;
  }

  static NotExistsToken() {
    return `Authorization - Bearer 토큰이 입력되지 않았습니다.`;
  }

  static ExpiredToken(info) {
    return `Authorization - Bearer 토큰이 만료되었습니다. ( ~ ${dayjs(
      info.expiredAt,
    ).format('YYYY-MM-DDTHH:mm:ssZ')})`;
  }
}
