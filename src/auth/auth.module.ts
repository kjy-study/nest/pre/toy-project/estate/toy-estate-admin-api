import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import * as dotenv from 'dotenv';
import { UsersModule } from '../users/users.module';
import { AuthService } from './auth.service';
import { LoginValidator } from './args/validator/login.validator';
import { JwtAuthStrategy } from './strategies/jwt-auth.strategy';
import { LocalAuthStrategy } from './strategies/local-auth.strategy';
import { AuthResolver } from './auth.resolver';
import { PrismaService } from '@mion/toy-estate-lib';
dotenv.config();

@Module({
  imports: [
    JwtModule.register({
      secret: `${process.env.JWT_SECRET}`,
      signOptions: {
        algorithm: 'HS512',
        expiresIn: '1h',
      },
    }),
    UsersModule,
  ],
  providers: [
    AuthService,
    LocalAuthStrategy,
    JwtAuthStrategy,
    LoginValidator,
    PrismaService,
    AuthResolver,
  ],
})
export class AuthModule {}
