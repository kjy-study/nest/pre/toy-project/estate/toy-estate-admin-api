import {
  CommonEntityBuilder,
  CommonValidateFunction,
} from '@mion/toy-estate-lib';
import { Field, InputType } from '@nestjs/graphql';
import { BooleanTypes, Prisma } from '@prisma/client';
import { IsEmail, IsString, Matches } from 'class-validator';
import { UserExceptionMessage } from '../../users/exceptions/users.exception.message';

/**
 * 로그인 요청 Input
 */
@InputType()
export class LoginInput
  implements CommonEntityBuilder<Prisma.UserFindFirstArgs>
{
  @Field((type) => String, { description: '이메일' })
  @IsEmail({ message: '이메일은 문자로 입력해 주십시오.' })
  email: string;

  @Field((type) => String, { description: '비밀번호' })
  @IsString({ message: '비밀번호는 문자로 입력해 주십시오.' })
  @Matches(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/, {
    message: UserExceptionMessage.InvalidPasswordForm(),
  })
  password: string;

  async build(): Promise<Prisma.UserFindFirstArgs> {
    const model: Prisma.UserFindFirstArgs = {
      where: {
        email: this.email,
        useYn: BooleanTypes.Y,
      },
    };
    return model;
  }

  async validate(validateFunction: CommonValidateFunction<any>): Promise<this> {
    await validateFunction.validate(this);
    return this;
  }
}
