import { CommonValidateFunction, BcryptHelper } from '@mion/toy-estate-lib';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { User } from '@prisma/client';
import { UsersCommonValidator } from '../../../users/args/validator/common/users-common.validator';
import { UserExceptionMessage } from '../../../users/exceptions/users.exception.message';
import { LoginInput } from '../login.input';

/**
 * 로그인 유효성 검사
 */
@Injectable()
export class LoginValidator implements CommonValidateFunction<LoginInput> {
  constructor(private readonly usersCommonValidator: UsersCommonValidator) {}

  async validate(input: LoginInput): Promise<Omit<User, 'password'>> {
    // email이 존재하는지 검사
    const savedUser = await this.usersCommonValidator.validateEmailExists(
      input.email,
    );

    // 비밀번호 검사
    if (!(await BcryptHelper.compare(input.password, savedUser.password))) {
      throw new UnauthorizedException(UserExceptionMessage.InvalidPassword());
    }
    const { password, ...result } = savedUser;

    return result;
  }
}
