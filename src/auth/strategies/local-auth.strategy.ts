import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { User } from '@prisma/client';
import { Strategy } from 'passport-local';
import { LoginInput } from '../args/login.input';
import { LoginValidator } from '../args/validator/login.validator';

@Injectable()
export class LocalAuthStrategy extends PassportStrategy(Strategy, 'local') {
  constructor(private readonly loginValidator: LoginValidator) {
    super({
      usernameField: 'email',
    });
  }

  async validate(
    email: string,
    password: string,
  ): Promise<Omit<User, 'password'>> {
    const input = new LoginInput();
    input.email = email;
    input.password = password;

    return await this.loginValidator.validate(input);
  }
}
