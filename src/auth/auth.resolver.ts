import { AllExceptionsGraphqlFilter } from '@mion/toy-estate-lib';
import { UseFilters } from '@nestjs/common';
import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { LoginInput } from './args/login.input';
import { AuthService } from './auth.service';
import { Public } from './meta/public.meta';
import { TokenModel } from './models/token.model';

@Resolver((of) => TokenModel)
@UseFilters(AllExceptionsGraphqlFilter)
export class AuthResolver {
  constructor(private readonly authService: AuthService) {}

  @Public()
  @Mutation((returns) => TokenModel, { description: 'JWT 토큰 발급' })
  async createToken(@Args('data') loginInput: LoginInput): Promise<TokenModel> {
    return await this.authService.createToken(loginInput);
  }
}
