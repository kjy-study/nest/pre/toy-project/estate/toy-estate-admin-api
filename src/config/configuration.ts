export const configuration = () => ({
  application: {
    port: process.env.PORT ?? 3000,
  },
  db: {
    type: process.env.DB_TYPE,
    host: process.env.DB_HOST,
    schema: process.env.DB_SCHEMA,
  },
  jwt: {
    secret: process.env.JWT_SECRET,
    expires: process.env.JWT_EXPIRES,
    refreshSecret: process.env.JWT_REFRESH_SECRET,
    refreshExpires: process.env.JWT_REFRESH_EXPIRES,
  },
  bcrypt: {
    saltRound: process.env.SALT_ROUND ?? 10,
  },
});
